﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 管理器接口
 *  en: todo:
 */


namespace JyFramework
{
    /// <summary>
    /// 管理器接口
    /// </summary>
    public interface IManager
    {
        /// <summary>
        /// 初始化
        /// </summary>
        void Init();

        /// <summary>
        /// 移除本管理器
        /// </summary>
        void Remove();

        /// <summary>
        /// 添加事件控制器
        /// </summary>
        void AddEventCtrl();

        /// <summary>
        /// 移除事件控制器
        /// </summary>
        void RemoveEventCtrl();

        /// <summary>
        /// 当前事件控制器
        /// </summary>
        EventController EventCtrl { get; }
    }
}