﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 事件使用接口
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 事件使用接口
    /// </summary>
    public interface IEventFunction
    {
        void AddEventCtrl(EventController eventCtrl);
        void RemoveCtrl(EventController eventCtrl);
        void RegistEvent(string name, MessageHandler msg);
        void NotifyEvent(string name, params object[] args);
        void RemoveEvent(string name, MessageHandler msg);
    }
}