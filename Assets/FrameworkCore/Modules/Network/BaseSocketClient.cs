﻿/*
 *  date: 2018-10-28
 *  author: John-chen
 *  cn: socket客户端
 *  en: todo:
 */

using System;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;

namespace JyFramework
{
    /// <summary>
    /// 基础socket客户端
    /// </summary>
    public class BaseSocketClient
    {
        public BaseSocketClient(string ip, int port, 
            AddressFamily address = AddressFamily.InterNetwork, 
            SocketType sType = SocketType.Stream, 
            ProtocolType pType = ProtocolType.Tcp)
        {
            _ip = ip;
            _port = port;

            _sType = sType;
            _pType = pType;
            _address = address;
            _socket = new Socket(address, _sType, _pType);

            Ctor();
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        public virtual void ConnectServer()
        {
            _socket.BeginConnect(_ip, _port, OnConnectedMsg, null);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msg"></param>
        public virtual void SendMsg(byte[] msg)
        {
            _socket.Send(msg);
        }

        /// <summary>
        /// 连接成功的回调
        /// </summary>
        /// <param name="ar"></param>
        protected void OnConnectedMsg(IAsyncResult ar)
        {
            _isConnected = true;
            _socket.EndConnect(ar);

            _socket.BeginReceive(_buffer, 0, _bufferSize, SocketFlags.None, OnReciveMsg, _socket);
        }

        /// <summary>
        /// 接收信息的回调
        /// </summary>
        /// <param name="ar"></param>
        protected void OnReciveMsg(IAsyncResult ar)
        {
            try
            {
                int len = _socket.EndReceive(ar);
                if (len > 0 && ar.IsCompleted)
                {
                    OnReadMsg(len, _buffer);
                }
                _socket.BeginReceive(_buffer, 0, _bufferSize, SocketFlags.None, OnReciveMsg, _socket);
            }
            catch (SocketException e)
            {
                _log?.Error("Socket accpet svrMsg error!" + e);
            }
        }

        /// <summary>
        /// 在线程中读取信息
        /// </summary>
        /// <param name="len"></param>
        /// <param name="buffer"></param>
        protected void OnReadMsg(int len, byte[] buffer)
        {
            if (_threadPool.Count == 0)
            {
                Thread td = new Thread(HandleMsgBegin);
                td.IsBackground = true;
                JySvrMsg svrMsg = new JySvrMsg(0, null, td);

                _threadPool.AddObject(svrMsg);
            }

            try
            {
                var jyMsg = _threadPool.GetObject();
                jyMsg.Len = len;
                jyMsg.Msg = buffer;
                jyMsg.Td.Start(jyMsg);
            }
            catch (ThreadAbortException e) { _log.Info(e); }
        }

        /// <summary>
        /// 开始操作msg
        /// </summary>
        /// <param name="msg"></param>
        protected void HandleMsgBegin(object msg)
        {
            lock (msg)
            {
                var jyMsg = (JySvrMsg)msg;
                HandleMsg(jyMsg);
            }
            HandleMsgEnd(msg);
        }

        /// <summary>
        /// 操作msg
        /// </summary>
        /// <param name="svrMsg"></param>
        protected virtual void HandleMsg(JySvrMsg svrMsg)
        {
             
        }

        /// <summary>
        /// 操作完成,回收对象
        /// </summary>
        /// <param name="msg"></param>
        protected void HandleMsgEnd(object msg)
        {
            try
            {
                var jyMsg = (JySvrMsg)msg;
                _threadPool.ReleaseObject(jyMsg);
            }
            catch(ThreadAbortException e) { _log.Info(e); }
        }

        /// <summary>
        /// 其他属性的初始化
        /// </summary>
        protected void Ctor()
        {
            _event = MessageCtrl.Ins.GetEventController();
            _log = JyApp.Ins.GetModule<LogModule>(ModuleName.Log)?.Log;
            _bufferSize = 1024 * 10;
            _buffer = new byte[_bufferSize];

            _threadPool = new InfinitePool<JySvrMsg>();
            _bufferPool = new InfinitePool<List<byte>>();
        }

        protected int _port;
        protected string _ip;
        protected Socket _socket;
        protected SocketType _sType;
        protected ProtocolType _pType;
        protected AddressFamily _address;
        
        protected byte[] _buffer;
        protected int _bufferSize;
        protected bool _isConnected;

        protected JyLog _log;
        protected EventController _event;
        protected InfinitePool<JySvrMsg> _threadPool;
        protected InfinitePool<List<byte>> _bufferPool;

        /// <summary>
        /// 线程操作需要的参数
        /// </summary>
        protected struct JySvrMsg
        {
            public int Len { get; set; }
            public byte[] Msg { get; set; }
            public Thread Td { get; set; }

            public JySvrMsg(int len, byte[] msg, Thread thread)
            {
                Len = 0;
                Msg = msg;
                Td = thread;
            }
        }
    }
}