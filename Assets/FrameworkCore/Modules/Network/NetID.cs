﻿/*
 *  date: 2018-10-28
 *  author: John-chen
 *  cn: 所有的协议,对应的协议号
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 所有的协议
    /// </summary>
    public enum NetID
    {
        Registr,        // 注册
        Login,          // 登录

    }
}