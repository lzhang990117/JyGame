﻿/*
 *  date: 2018-05-17
 *  author: John-chen
 *  cn: 引用计数控制器
 *  en: todo:
 */

using JyFramework;
using UnityEngine;

/// <summary>
/// 引用计数专用
/// </summary>
public class RefObjHandle : MonoBehaviour
{
    /// <summary>
    /// 资源名
    /// </summary>
    public string ResName
    {
        get { return _resName; }
    }

    /// <summary>
    /// 设置资源名并且发送消息
    /// </summary>
    /// <param name="resName"></param>
    public void SetResName(string resName)
    {
        _resName = resName;
        MessageCtrl.Ins.NotifyEvent(ResEvent.OnAddRef, _resName);
    }

    /// <summary>
    /// 在克隆的时候通知增加引用
    /// </summary>
    private void Awake()
    {
        if(!string.IsNullOrEmpty(_resName))
            MessageCtrl.Ins.NotifyEvent(ResEvent.OnAddRef, _resName);
    }

    /// <summary>
    /// 在删除时通知减少引用
    /// </summary>
    private void OnDestroy()
    {
        MessageCtrl.Ins.NotifyEvent(ResEvent.OnReduceRef, _resName);
    }

    /// <summary>
    /// 资源名
    /// </summary>
    [SerializeField]
    private string _resName;
}

