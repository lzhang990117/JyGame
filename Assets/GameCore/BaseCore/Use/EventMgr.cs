﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 客户端使用的事件管理器
 *  en: todo:
 */

using JyFramework;

namespace GameCore.BaseCore
{
    /// <summary>
    /// 客户端使用的事件管理器
    /// </summary>
    public class EventMgr : Singleton<EventMgr>, IEventFunction
    {
        public EventController GetEventController(string name = "TempEventController")
        {
            return MessageCtrl.Ins.GetEventController(name);
        }

        public void AddEventCtrl(EventController eventCtrl)
        {
            MessageCtrl.Ins.AddEventCtrl(eventCtrl);
        }

        public void NotifyEvent(string name, params object[] args)
        {
            MessageCtrl.Ins.NotifyEvent(name, args);
        }
         
        public void RegistEvent(string name, MessageHandler msg)
        {
            MessageCtrl.Ins.RegistEvent(name, msg);
        }

        public void RemoveCtrl(EventController eventCtrl)
        {
            MessageCtrl.Ins.RemoveCtrl(eventCtrl);
        }

        public void RemoveEvent(string name, MessageHandler msg)
        {
            MessageCtrl.Ins.RemoveEvent(name, msg);
        }
    }
}